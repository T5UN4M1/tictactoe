#pragma once
class Point
{
public:
	Point();
	Point(unsigned x, unsigned y);

	inline bool operator==(const Point& pt);
	inline bool operator<(const Point& pt);
	inline const Point& operator+(const Point& pt);
	inline const Point& operator+=(const Point& pt);
	unsigned x;
	unsigned y;
};


bool Point::operator==(const Point & pt)
{
	return this->x == pt.x && this->y == pt.y;
}

bool Point::operator<(const Point & pt)
{
	return this->x < pt.x && this->y < pt.y;
}

const Point & Point::operator+(const Point & pt)
{
	return Point(x + pt.x, y + pt.y);
}

const Point & Point::operator+=(const Point & pt)
{
	this->x += pt.x;
	this->y += pt.y;
	return *this;
}