#pragma once
#include <bitset>
#include <vector>
#include <string>
#include <iostream>

#include "Point.h"

using std::vector;
using std::pair;
using std::make_pair;
using std::string;
using std::cout;
using std::endl;


enum Cell {
	EMPTY = 0,
	O = 1,
	X = 2
};
enum State {
	PLAYING,
	OVER
};
class Game
{
protected:
	// game
	unsigned turn; // who has to play ? turn%2==0 == true -> O , false -> X 
	vector<Cell> grid;

	// config
	Point gridSize;
	unsigned winSize;

	// endState
	State state;
	Cell winner;
	vector<Point> winningLine;

public:
	
	Game(unsigned xSize,unsigned ySize,unsigned winSize=3); // create game with board size
	Game(const Game& game); // copy

	bool play(unsigned x, unsigned y) { // play to x,y , returns true if if play is legit and turn is over
		if (!isOver() && set(x, y, turnSymbol())) {
			checkVictory(x,y);
			checkOver();
			++turn;
			return true;
		}
		return false;
	}
	Cell get(unsigned x, unsigned y)const { // return cell content
		return (isInside(x, y)) ? grid[coord(x, y)] : EMPTY;
	}

	Cell turnSymbol() { // get current turn symbol
		return turnSymbol(turn);
	}
	// return symbol
	Cell turnSymbol(unsigned turn) {
		return turn%2==0 ? O : X;
	}
	// is the game over ?
	bool isOver()const{
		return state == OVER;
	}
	Cell getWinner()const {
		return winner;
	}
	void toConsole() {
		for (unsigned y = 0; y < gridSize.y; ++y) {
			for (unsigned x = 0; x < gridSize.x; ++x) {
				switch (get(x, y)) {
				case O:cout << "O"; break;
				case X:cout << "X"; break;
				case EMPTY:cout << "_"; break;
				}
			}
			cout << endl;
		}
	}

protected :

	void checkVictory(unsigned x, unsigned y);
	void checkOver() {
		if (turn == gridSize.x*gridSize.y) {
			state = OVER;
		}
	}
	bool isInside(unsigned x, unsigned y)const {
		return Point(x,y)<gridSize;
	}
	bool set(unsigned x, unsigned y, Cell c) { // setter
		if (!isInside(x,y) or get(x,y) != EMPTY) {
			return false;
		}
		grid[coord(x, y)] = c;
		return true;
	}
	const Point& coord(unsigned o)const {
		return Point(o %gridSize.x, o / gridSize.y);
	}
	unsigned coord(unsigned x, unsigned y)const {
		return y * gridSize.x + x;
	}


};

