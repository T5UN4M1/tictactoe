#include "Game.h"



Game::Game(const Game& game)
{
	this->turn = game.turn;
	this->gridSize = game.gridSize;
	this->winSize = game.winSize;

	this->grid.clear();
	for (const Cell& c : game.grid) {
		this->grid.push_back(Cell(c));
	}
	this->winningLine.clear();
	for (const Point& p : game.winningLine) {
		this->winningLine.push_back(Point(p));
	}
}



Game::Game(unsigned xSize, unsigned ySize,unsigned winSize):winSize(winSize)
{
	grid.resize(xSize * ySize, EMPTY);
	gridSize.x = xSize;
	gridSize.y = ySize;
	state = PLAYING;
	winner = EMPTY;
	winningLine.clear();
	
}


void Game::checkVictory(unsigned x, unsigned y){
	Cell cType = get(x, y);
	if (cType == EMPTY ||state == OVER) { // this shouldnt happen
		return;
	}
	/* we want to test :
	(1,0) -> -
	(0,1) -> |
	(1,1) -> /
	(-1,1)-> \

	then we give them a multiplicator to test the entire line until both end are out of grid or until victory is detected
	*/
	Point origin(x,y);

	vector<Point> ttt;
	ttt.push_back(Point(1, 0));
	ttt.push_back(Point(0, 1));
	ttt.push_back(Point(1, 1));
	ttt.push_back(Point(-1, 1));
	for (const Point& p : ttt) {
		unsigned s = 1;
		for (int i = -1; i < 2; i += 2) {
			Point t(p.x*i, p.y*i);
			Point scan(origin);
			scan += t;
			
			while (get(scan.x, scan.y) == cType) {
				scan += t;
				++s;
			}
		}
		if (s >= winSize) {
			winningLine.clear();
			winningLine.push_back(origin);
			// redoing it to get winning line
			for (int i = -1; i < 2; i += 2) {
				Point scan(origin);
				Point t(p.x*i, p.y*i);
				scan += t;

				while (get(scan.x, scan.y) == cType) {
					winningLine.push_back(scan);
					scan += t;
					++s;
				}
			}
			winner = cType;
			state = OVER;
			return;
		}
	}
}